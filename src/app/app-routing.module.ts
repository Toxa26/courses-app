import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizedGuard } from './auth/guards/authorized.guard';
import { NonAuthorizedGuard } from './auth/guards/non-authorized.guard';

const routes: Routes = [
  {
    path: 'courses',
    loadChildren: () => import('@features/courses/courses.module').then((m) => m.CoursesModule)
  },
  {
    path: 'courses/edit/:id',
    loadChildren: () => import('@features/course-form/course-form.module').then((m) => m.CourseFormModule),
    canLoad: [AuthorizedGuard]
  },
  {
    path: 'courses/add',
    loadChildren: () => import('@features/course-form/course-form.module').then((m) => m.CourseFormModule),
    canLoad: [AuthorizedGuard]
  },
  {
    path: 'account',
    loadChildren: () => import('@features/personal-account/personal-account.module')
      .then((m) => m.PersonalAccountModule),
    canLoad: [AuthorizedGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('@features/login/login.module').then((m) => m.LoginModule),
    canActivate: [NonAuthorizedGuard]
  },
  {
    path: 'registration',
    loadChildren: () => import('@features/registration/registration.module').then((m) => m.RegistrationModule),
    canActivate: [NonAuthorizedGuard]
  },
  { path: '**', redirectTo: 'courses', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
