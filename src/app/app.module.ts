import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { APP_ICONS, DEFAULT_ICONS } from './shared/theme/icons';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from '@shared';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [{ provide: APP_ICONS, useValue: DEFAULT_ICONS }],
  bootstrap: [AppComponent],
})
export class AppModule {
}

/** USEFUL MATERIALS
 * 1) File path shortcuts - https://fireship.io/snippets/improving-long-relative-paths-imports/
 * 2) Injection tokens - https://angular.io/api/core/InjectionToken
 */


/** My questions|problems
 * 1) Simple routing working well, but with lazy loading I've problems
 * 2) How to set value which we get from http client to FormArray(authors in course-from,
 * now setting only first elem of array)
 * 3) Where should we transform array of authors id to authors name? Is it correct implementing now in
 * courses.component.ts - 32 line
 * 4) How correctly working with server?
 * 5) How to receive all possibles endpoints and methods which I should used for them?
 * 6) Now deleting of course isn't working(403: Forbidden). As I now, I should to use some TOKEN, how I should
 * generate it and sent to have possibility to delete/add/edit data?
 * 7) How to connect services to store service and using it in components?
 * 8) Why I need, for instance, author$$ BehaviorSubject and author$ Observable? What they should do?
 * 9) Why I need isLoading$$?
*/
