import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IUser } from '../../../auth/auth.models';
import { UserService } from '../api/user.service';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserStoreService {
  readonly user$$ = new BehaviorSubject<IUser>(null);
  readonly loading$$ = new BehaviorSubject<boolean>(false);

  readonly user$ = this.user$$.asObservable();

  constructor(private userService: UserService) { }

  getUser(ignoreCache = false): Observable<IUser> {
    if (ignoreCache || !this.user$$.value) {
      this.loading$$.next(true);
      return this.userService.getUser()?.pipe(
        finalize(() => this.loading$$.next(false)),
        tap(user => this.user$$.next(user))
      );
    } else {
      return this.user$;
    }
  }
}
