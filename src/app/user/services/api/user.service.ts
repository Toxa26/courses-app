import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionStoreService } from '../../../auth/services/session-store.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IUser } from '../../../auth/auth.models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, private sessionStoreService: SessionStoreService) {}

  getUser(): Observable<IUser> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.get(`http://localhost:3000/users/me`, { headers }).pipe(
      map((response: { successful: boolean; result: IUser }) => response.result)
    );
  }
}
