import { faEye, faEyeSlash, faPen, faTrash, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { InjectionToken } from '@angular/core';

export interface IAppIcons {
  [key: string]: IconDefinition;
}

export const DEFAULT_ICONS: IAppIcons = {
  pen: faPen,
  trash: faTrash,
  eye: faEye,
  eyeSlash: faEyeSlash,
};

export const APP_ICONS = new InjectionToken<IAppIcons>('APP_ICONS');
