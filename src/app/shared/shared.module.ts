import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent, HeaderComponent, InfoComponent, SearchComponent } from './components';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MinutesToHoursPipe } from './pipes/minutes-to-hours.pipe';
import { CustomEmailValidatorDirective } from './directives/custom-email-validator.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StringJoinerPipe } from './pipes/string-joiner.pipe';
import { CreationDatePipe } from './pipes/creation-date.pipe';
import { TogglePasswordModule } from './directives/toggle-password';
import { RouterModule } from '@angular/router';

const PIPES = [MinutesToHoursPipe, CreationDatePipe, StringJoinerPipe];
const MODULES = [CommonModule, FontAwesomeModule, FormsModule, ReactiveFormsModule];
const DIRECTIVES = [CustomEmailValidatorDirective];
const COMPONENTS = [HeaderComponent, ButtonComponent, InfoComponent, SearchComponent];

@NgModule({
  declarations: [...PIPES, ...DIRECTIVES, ...COMPONENTS],
  providers: [...PIPES, ...DIRECTIVES],
  imports: [...MODULES, RouterModule],
  exports: [
    ...PIPES,
    ...MODULES,
    ...DIRECTIVES,
    ...COMPONENTS,
    TogglePasswordModule
  ],
})
export class SharedModule {
}
