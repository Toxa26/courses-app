import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringJoiner'
})
export class StringJoinerPipe implements PipeTransform {
  transform(value: unknown[], separator = ''): string {
    return value.join(separator);
  }
}
