import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'creationDate'
})
export class CreationDatePipe implements PipeTransform {
  transform(date: string, format = 'yyyy-MM-dd'): string {
    const day = Number(date.split('/')[0]);
    const month = Number(date.split('/')[1]) - 1;
    const year = Number(date.split('/')[2]);
    const convertedDate = new Date(year, month, day);

    convertedDate.setDate(convertedDate.getDate());
    return new DatePipe('en-US').transform(convertedDate, format);
  }
}
