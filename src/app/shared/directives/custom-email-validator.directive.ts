import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[appCustomEmailValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: CustomEmailValidatorDirective,
    multi: true,
  }],
})
export class CustomEmailValidatorDirective implements Validator {
  validate(control: AbstractControl): { [key: string]: boolean } | null {
    const emailRegExp = /\S+@\S+\.\S+/;

    return emailRegExp.test(control.value) ? null : { emailIsInvalid: true };
  }
}
