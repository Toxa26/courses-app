import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TogglePasswordComponent } from './toggle-password.component';
import { TogglePasswordTypeDirective } from './toggle-password-type.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  declarations: [
    TogglePasswordComponent,
    TogglePasswordTypeDirective
  ],
  exports: [TogglePasswordTypeDirective]
})
export class TogglePasswordModule { }
