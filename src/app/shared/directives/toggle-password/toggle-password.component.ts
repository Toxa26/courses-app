import { Component, Inject, HostListener } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { APP_ICONS, IAppIcons } from '../../theme/icons';
import { map } from 'rxjs/operators';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  templateUrl: './toggle-password.component.html',
  styleUrls: ['./toggle-password.component.css']
})
export class TogglePasswordComponent {
  readonly isToggled$ = new BehaviorSubject<boolean>(false);
  readonly icon$: Observable<IconDefinition> = this.isToggled$
    .pipe(map(isToggled => isToggled ? this.ICONS.eye : this.ICONS.eyeSlash));

  @HostListener('click')
  toggle(): void {
    this.isToggled$.next(!this.isToggled$.value);
  }

  constructor(@Inject(APP_ICONS) readonly ICONS: IAppIcons) { }
}
