import {
  Directive,
  ElementRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  OnInit,
  ComponentRef,
  Renderer2, OnDestroy
} from '@angular/core';
import { TogglePasswordComponent } from './toggle-password.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appTogglePasswordType]'
})
export class TogglePasswordTypeDirective implements OnInit, OnDestroy {
  toggleBtnRef: ComponentRef<TogglePasswordComponent>;

  private readonly destroyed$ = new Subject();

  constructor(private element: ElementRef,
              private viewContainerRef: ViewContainerRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(TogglePasswordComponent);

    this.toggleBtnRef = this.viewContainerRef.createComponent(componentFactory);
    this.renderer.appendChild(
      this.element.nativeElement.parentNode,
      this.toggleBtnRef.location.nativeElement,
    );

    this.toggleBtnRef.instance.isToggled$
      .pipe(takeUntil(this.destroyed$))
      .subscribe(isToggled => isToggled
        ? this.renderer.setAttribute(this.element.nativeElement, 'type', 'text')
        : this.renderer.setAttribute(this.element.nativeElement, 'type', 'password')
      );
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
