import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  @Input() placeholder: string;
  @Output() getCoursesBySearch = new EventEmitter<{
    search: string;
  }>();

  onSearch(item): void {
    this.getCoursesBySearch.emit(item);
  }
}
