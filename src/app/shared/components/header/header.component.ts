import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { UserStoreService } from '../../../user/services';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnDestroy {
  loginText = 'Login';
  registerText = 'Register';
  logoutText = 'Logout';

  // TODO: Doesn't work after login, only if I reload the page after login
  myProfileText$ = this.userStoreService.getUser()?.pipe(
    map((user) => {
      return 'My profile, ' + user.name;
    })
  );

  private readonly destroyed$ = new Subject<boolean>();

  constructor(public authService: AuthService, private userStoreService: UserStoreService) {
    this.userStoreService.getUser()?.pipe(takeUntil(this.destroyed$)).subscribe();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  logout(): void {
    this.authService.logout().subscribe();
  }
}
