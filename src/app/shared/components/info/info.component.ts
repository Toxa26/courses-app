import { Component, Input } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent {
  @Input() title: string;
  @Input() text: string;
  @Input() buttonText: string;

  constructor(public authService: AuthService) {
  }
}
