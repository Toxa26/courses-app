import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionStoreService } from './session-store.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ILoginResponse, IRegisterResponse, IUser } from '../auth.models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  readonly isAuthorized$$ = new BehaviorSubject<boolean>(false);
  readonly currentUser$$ = new BehaviorSubject<IUser>(null);

  readonly isAuthorized$ = this.isAuthorized$$.asObservable();

  constructor(private sessionStoreService: SessionStoreService,
              private http: HttpClient,
              private router: Router) {
    if (this.sessionStoreService.getToken()) {
      this.isAuthorized$$.next(true);
    }
  }

  login(email: string, password: string): Observable<ILoginResponse> {
    return this.http.post(`http://localhost:3000/login`, { email, password }).pipe(
      tap((response: ILoginResponse) => {
        if (response.successful) {
          this.isAuthorized$$.next(true);
          this.currentUser$$.next(response.user);
          this.sessionStoreService.setToken(response.result);
          this.router.navigate(['courses']);
        }
      })
    );
  }

  register(name: string, email: string, password: string): Observable<IRegisterResponse> {
    return this.http.post<IRegisterResponse>(`http://localhost:3000/register`, { name, email, password }).pipe(
      tap((response: IRegisterResponse) => {
        if (response.successful) {
          this.router.navigate(['courses']);
        }
      })
    );
  }

  logout(): Observable<void> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.delete<void>(`http://localhost:3000/logout`, { headers }).pipe(
      tap(() => {
        this.isAuthorized$$.next(false);
        this.currentUser$$.next(null);
        this.sessionStoreService.deleteToken();
        this.router.navigate(['login']);
      })
    );
  }
}
