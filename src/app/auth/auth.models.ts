export interface IUser {
  name: string;
  email: string;
}

export interface ILoginResponse {
  successful: boolean;
  result: string;
  user: IUser;
}

export interface IRegisterResponse {
  successful: boolean;
  result: string;
}

export interface IAuthor {
  name: string;
  id: string;
}

export interface IAuthorResponse {
  successful: boolean;
  result: IAuthor;
}

export interface IUser {
  name: string;
  email: string;
  password: string;
  role: string;
  id: string;
}
