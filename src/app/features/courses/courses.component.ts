import { Component, OnDestroy } from '@angular/core';
import { ICourseCard } from '@features/course/course.models';
import { AuthorsStoreService, CoursesStoreService } from '@services/store';
import { Observable, combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AuthService } from '@auth/services/auth.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnDestroy {
  readonly courses$ = this.getCourses();
  readonly hasCourses$ = this.courses$.pipe(map(courses => courses.length > 0));

  private readonly destroyed$ = new Subject<boolean>();

  constructor(private authorsStoreService: AuthorsStoreService,
              private coursesStoreService: CoursesStoreService,
              public authService: AuthService,
  ) {
    this.authorsStoreService.getAuthors().pipe(takeUntil(this.destroyed$)).subscribe();
    this.coursesStoreService.getCourses().pipe(takeUntil(this.destroyed$)).subscribe();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  onDeleteCourse(id: string): void {
    this.coursesStoreService.deleteCourse(id).subscribe();
  }

  getCourses(): Observable<ICourseCard[]> {
    return combineLatest([
      this.coursesStoreService.courses$,
      this.authorsStoreService.authors$
    ]).pipe(
      map(([courses, authors]) =>
        courses.map((course: ICourseCard) => ({
            ...course,
            authors: course.authors.map(authorId => authors
              .find(author => author.id === authorId)?.name).filter(author => author !== undefined)
          })
        )
      )
    );
  }
}
