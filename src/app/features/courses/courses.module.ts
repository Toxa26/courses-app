import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { CoursesComponent } from './courses.component';
import { CourseModule } from '@features/course';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {
    path: '',
    component: CoursesComponent,
  }
];

@NgModule({
  declarations: [CoursesComponent],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
    CourseModule
  ],
  exports: [CoursesComponent],
})
export class CoursesModule {
}
