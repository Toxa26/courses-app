import { NgModule } from '@angular/core';
import { LoginComponent } from '@features/login/login.component';
import { SharedModule } from '@shared';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {
    path: '',
    component: LoginComponent,
  }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
  ],
  exports: [LoginComponent],
})
export class LoginModule {
}
