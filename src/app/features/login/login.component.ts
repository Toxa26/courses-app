import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { UserStoreService } from '../../user/services';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnDestroy {
  private readonly destroyed$ = new Subject<boolean>();

  constructor(private authService: AuthService, public userStoreService: UserStoreService) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }


  login(user): void {
    this.authService.login(user.email, user.password).pipe(takeUntil(this.destroyed$)).subscribe(() => {
      this.userStoreService.getUser()?.pipe(takeUntil(this.destroyed$)).subscribe();
    });
  }
}
