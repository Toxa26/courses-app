import { Component, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable, Subject } from 'rxjs';
import { ICourseCard } from '@features/course/course.models';
import { map, takeUntil, tap } from 'rxjs/operators';
import { AuthorsStoreService, CoursesStoreService } from '@services/store';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css'],
})
export class CourseFormComponent implements OnDestroy {
  course$: Observable<ICourseCard>;

  private readonly destroyed$$ = new Subject<boolean>();
  private readonly addMode = this.router.url.includes('add');
  private readonly editMode = this.router.url.includes('edit');

  newCourseForm: FormGroup;
  duration: number;
  id: string;

  constructor(private authorsStoreService: AuthorsStoreService,
              private coursesStoreService: CoursesStoreService,
              private activateRoute: ActivatedRoute,
              private router: Router) {
    if (this.editMode) {
      this.id = this.activateRoute.snapshot.paramMap.get('id');
      this.authorsStoreService.getAuthors().pipe(takeUntil(this.destroyed$$)).subscribe();
      this.coursesStoreService.getCourseById(this.id).pipe(takeUntil(this.destroyed$$)).subscribe();

      this.course$ = this.transformCourseFormData(true);
    }

    if (this.addMode) {
      this.course$ = this.transformCourseFormData(false);
    }

    this.newCourseForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      newAuthor: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[a-zA-Z0-9 ]+'),
      ])),
      authors: new FormArray([]),
      duration: new FormControl('', Validators.compose([
        Validators.required,
        Validators.min(0),
      ])),
    });
  }

  ngOnDestroy(): void {
    this.destroyed$$.next(true);
    this.destroyed$$.complete();
  }

  transformCourseFormData(fillFrom: boolean): Observable<ICourseCard> {
    return combineLatest([
      this.coursesStoreService.courseById$,
      this.authorsStoreService.authors$
    ]).pipe(
      map(([course, authors]) => {
        return {
          ...course,
          authors: course?.authors.map(authorId => authors.filter(author => author !== undefined)
            .find(author => author.id === authorId)?.name)
            .filter(author => author)
        };
      }),
      tap((course) => {
        if (fillFrom) {
          this.newCourseForm.patchValue({
            title: course.title,
            description: course.description,
            duration: course.duration,
          });

          this.resetAuthors();
          course.authors?.forEach(author => {
            (this.newCourseForm.controls.authors as FormArray)
              .push(new FormControl({ value: author, disabled: true }));
          });

          this.duration = course.duration;
        }
      })
    );
  }

  getAuthorsIdByName(name: string): Observable<string> {
    return this.authorsStoreService.getAuthors().pipe(
      map(authors => authors.find(author => author.name === name)?.id)
    );
  }

  get authors(): FormArray {
    return this.newCourseForm.controls.authors as FormArray;
  }

  resetAuthors(): void {
    (this.newCourseForm.controls.authors as FormArray).clear();
  }

  confirm(item): void {
    const course = {
      ...item,
      authors: [item.newAuthor]
    };

    if (this.editMode) {
      this.getAuthorsIdByName(course.authors[0]).subscribe((id) => {
        course.authors = [id];
        this.coursesStoreService.updateCourse(course, this.id).subscribe();
      });
      return;
    }

    if (this.addMode) {
      this.getAuthorsIdByName(course.authors[0]).subscribe((id) => {
        course.authors = [id];
        this.coursesStoreService.addCourse(course).subscribe();
      });
      return;
    }
  }

  onAuthorAdd(author): void {
    this.authorsStoreService.addAuthor(author).subscribe(() => {
      alert('Author was added');
    });
  }

  onAuthorRemove(author: string): void {
    this.getAuthorsIdByName(author).subscribe((id) => {
      this.authorsStoreService.deleteAuthor(id).subscribe();

      this.transformCourseFormData(true);
    });
  }

  setDuration(event: any): void {
    this.duration = event.target.value;
  }

  isInvalid(field: string): boolean {
    return (this.newCourseForm.get(field).touched || this.newCourseForm.get(field).dirty) &&
      !this.newCourseForm.get(field).valid;
  }

  isFormValid(): boolean {
    return (this.newCourseForm.controls.title as FormControl).valid
      && (this.newCourseForm.controls.description as FormControl).valid
      && (this.newCourseForm.controls.newAuthor as FormControl).valid
      && (this.newCourseForm.controls.duration as FormControl).valid;
  }
}
