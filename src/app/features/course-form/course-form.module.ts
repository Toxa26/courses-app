import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { CourseFormComponent } from '@features/course-form/course-form.component';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {
    path: '',
    component: CourseFormComponent,
  }
];

@NgModule({
  declarations: [CourseFormComponent],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
  ],
  exports: [CourseFormComponent],
})
export class CourseFormModule {
}
