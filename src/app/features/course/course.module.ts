import { NgModule } from '@angular/core';
import { CourseComponent } from './course.component';
import { SharedModule } from '@shared';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CourseComponent],
  imports: [
    SharedModule,
    RouterModule
  ],
  exports: [CourseComponent],
})
export class CourseModule {
}
