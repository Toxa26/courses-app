import { ICourseCard } from '@features/course/course.models';

export const mockCourseCard1: ICourseCard = {
  title: 'title',
  description: 'description',
  creationDate: '9/3/2021',
  duration: 30,
  authors: ['9b87e8b8-6ba5-40fc-a439-c4e30a373d36'],
  id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467'
};

export const mockCourseCard2: ICourseCard = {
  title: 'title',
  description: 'description',
  creationDate: '9/3/2021',
  duration: 30,
  authors: ['9b87e8b8-6ba5-40fc-a439-c4e30a373d36'],
  id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467'
};

export const mockCourseCard3: ICourseCard = {
  title: 'title',
  description: 'description',
  creationDate: '9/3/2021',
  duration: 30,
  authors: ['9b87e8b8-6ba5-40fc-a439-c4e30a373d36'],
  id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467'
};

export const mockCourseList: ICourseCard[] = [mockCourseCard1, mockCourseCard2, mockCourseCard3];
