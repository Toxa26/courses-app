export interface ICourseCard {
  title: string;
  description: string;
  creationDate: Date | string;
  duration: number;
  authors: string[];
  id: string;
}

export interface ICourseCardRequest {
  title: string;
  description: string;
  duration: number;
  authors: string[];
}

export interface ICourseCardResponse {
  successful: boolean;
  result: ICourseCard;
}

export interface IAuthor {
  name: string;
  id: string;
}
