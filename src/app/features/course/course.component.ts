import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { ICourseCard } from '@features/course/course.models';
import { APP_ICONS, IAppIcons } from '../../shared/theme/icons';
import { AuthService } from '@auth/services/auth.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
})
export class CourseComponent {
  @Input() course: ICourseCard;
  @Output() deleteId = new EventEmitter<string>();

  constructor(@Inject(APP_ICONS) readonly ICONS: IAppIcons,
              public authService: AuthService) {}

  deleteCourse(id: string): void {
    this.deleteId.emit(id);
  }
}
