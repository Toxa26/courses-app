import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { UserStoreService } from '../../user/services';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-personal-account',
  templateUrl: './personal-account.component.html',
  styleUrls: ['./personal-account.component.css']
})
export class PersonalAccountComponent implements OnDestroy {
  private readonly destroyed$ = new Subject<boolean>();

  constructor(public userStoreService: UserStoreService) {
    this.userStoreService.getUser().pipe(takeUntil(this.destroyed$)).subscribe();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
