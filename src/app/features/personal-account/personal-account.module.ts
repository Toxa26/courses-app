import { NgModule } from '@angular/core';
import { PersonalAccountComponent } from './personal-account.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared';

const ROUTES: Routes = [
  {
    path: '',
    component: PersonalAccountComponent,
  }
];

@NgModule({
  declarations: [PersonalAccountComponent],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
  ],
  exports: [PersonalAccountComponent]
})
export class PersonalAccountModule { }
