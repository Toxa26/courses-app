import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomEmailValidatorDirective } from '../../shared/directives/custom-email-validator.directive';
import { AuthService } from '@auth/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {
  registerForm: FormGroup;

  constructor(private customEmailValidatorDirective: CustomEmailValidatorDirective,
              private authService: AuthService) {
    this.registerForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        this.customEmailValidatorDirective.validate,
      ])),
      password: new FormControl('', Validators.required),
    });
  }

  isInvalid(field: string): boolean {
    return (this.registerForm.get(field).touched || this.registerForm.get(field).dirty) &&
      !this.registerForm.get(field).valid;
  }

  register(user): void {
    this.authService.register(user.name, user.email, user.password).subscribe();
  }
}
