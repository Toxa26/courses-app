import { NgModule } from '@angular/core';
import { RegistrationComponent } from '@features/registration/registration.component';
import { SharedModule } from '@shared';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {
    path: '',
    component: RegistrationComponent,
  }
];

@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
  ],
  exports: [RegistrationComponent],
})
export class RegistrationModule {
}
