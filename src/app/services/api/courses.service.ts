import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICourseCard, ICourseCardRequest, ICourseCardResponse } from '@features/course/course.models';
import { map } from 'rxjs/operators';
import { SessionStoreService } from '../../auth/services/session-store.service';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  constructor(private http: HttpClient, private sessionStoreService: SessionStoreService) {
  }

  getAll(): Observable<ICourseCard[]> {
    return this.http.get(`http://localhost:3000/courses/all`).pipe(
      map((response: { successful: boolean; result: ICourseCard[] }) => response.result)
    );
  }

  addCourse(course: ICourseCardRequest): Observable<ICourseCard> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.post(`http://localhost:3000/courses/add`, {
      title: course.title,
      description: course.description,
      duration: course.duration,
      authors: course.authors,
    }, { headers }).pipe(
      map((response: ICourseCardResponse) => response.result));
  }

  updateCourse(course: ICourseCardRequest, id: string): Observable<ICourseCard> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.put(`http://localhost:3000/courses/${id}`, {
      title: course.title,
      description: course.description,
      duration: course.duration,
      authors: course.authors,
    }, { headers }).pipe(
      map((response: ICourseCardResponse) => response.result));
  }

  deleteCourse(id: string): Observable<void> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.delete<void>(`http://localhost:3000/courses/${id}`, { headers });
  }

  getCourse(id: string): Observable<ICourseCard> {
    return this.http.get(`http://localhost:3000/courses/${id}`).pipe(
      map((response: { successful: boolean; result: ICourseCard }) => response.result)
    );
  }
}
