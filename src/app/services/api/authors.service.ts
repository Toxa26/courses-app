import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAuthor } from '@features/course/course.models';
import { map } from 'rxjs/operators';
import { SessionStoreService } from '../../auth/services/session-store.service';
import { IAuthorResponse } from '../../auth/auth.models';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  constructor(private http: HttpClient, private sessionStoreService: SessionStoreService) {
  }

  getAll(): Observable<IAuthor[]> {
    return this.http.get(`http://localhost:3000/authors/all`).pipe(
      map((response: { successful: boolean; result: IAuthor[] }) => response.result)
    );
  }

  addAuthor(name: string): Observable<IAuthor> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.post(`http://localhost:3000/authors/add`, { name }, { headers }).pipe(
      map((response: IAuthorResponse) => response.result));
  }

  deleteAuthor(id: string): Observable<void> {
    const token = this.sessionStoreService.getToken();
    const headers = new HttpHeaders().set('Authorization', token);

    if (!token) return;

    return this.http.delete<void>(`http://localhost:3000/authors/${id}`, { headers });
  }
}
