import { Injectable } from '@angular/core';
import { ICourseCard } from '@features/course/course.models';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { CoursesService } from '../api';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CoursesStoreService {
  private readonly courses$$ = new BehaviorSubject<ICourseCard[]>([]);
  private readonly courseById$$ = new BehaviorSubject<ICourseCard>(null);
  private readonly loading$$ = new BehaviorSubject<boolean>(false);

  readonly courses$ = this.courses$$.asObservable();
  readonly courseById$ = this.courseById$$.asObservable();
  readonly loading$ = this.loading$$.asObservable();

  constructor(private readonly coursesService: CoursesService,
              private router: Router) { }

  getCourses(ignoreCache = false): Observable<ICourseCard[]> {
    if (ignoreCache || this.courses$$.value.length === 0) {
      this.loading$$.next(true);
      return this.coursesService.getAll().pipe(
        finalize(() => this.loading$$.next(false)),
        tap(courses => this.courses$$.next(courses))
      );
    } else {
      return this.courses$;
    }
  }

  getCourseById(id: string, ignoreCache = false): Observable<ICourseCard> {
    if (ignoreCache || !this.courseById$$.value || this.courseById$$.value.id !== id) {
      this.loading$$.next(true);
      return this.coursesService.getCourse(id).pipe(
        finalize(() => this.loading$$.next(false)),
        tap(course => this.courseById$$.next(course))
      );
    } else {
      return this.courseById$;
    }
  }

  addCourse(course: ICourseCard): Observable<ICourseCard> {
    this.loading$$.next(true);
    return this.coursesService.addCourse(course).pipe(
      finalize(() => this.loading$$.next(false)),
      tap((newCourse) => {
        this.courses$$.next(this.courses$$.value.concat(newCourse));
        this.router.navigate(['courses']);
      })
    );
  }

  updateCourse(course: ICourseCard, id: string): Observable<ICourseCard> {
    this.loading$$.next(true);
    return this.coursesService.updateCourse(course, id).pipe(
      finalize(() => this.loading$$.next(false)),
      tap((newCourse) => {
        this.courses$$.next(this.courses$$.value.map(c => (c.id === newCourse.id ? newCourse : c)));
        this.router.navigate(['courses']);
      })
    );
  }

  deleteCourse(id: string): Observable<void> {
    this.loading$$.next(true);
    return this.coursesService.deleteCourse(id).pipe(
      finalize(() => this.loading$$.next(false)),
      tap(() => this.courses$$.next(this.courses$$.value.filter(c => c.id !== id)))
    );
  }
}
