import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IAuthor } from '@features/course/course.models';
import { AuthorsService } from '../api';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorsStoreService {
  private readonly authors$$ = new BehaviorSubject<IAuthor[]>([]);
  readonly loading$$ = new BehaviorSubject<boolean>(false);

  readonly authors$ = this.authors$$.asObservable();

  constructor(private readonly authorsService: AuthorsService) { }

  getAuthors(ignoreCache = false): Observable<IAuthor[]> {
    if (ignoreCache || this.authors$$.value.length === 0) {
      this.loading$$.next(true);
      return this.authorsService.getAll().pipe(
        finalize(() => this.loading$$.next(false)),
        tap(authors => this.authors$$.next(authors))
      );
    } else {
      return this.authors$;
    }
  }

  deleteAuthor(id: string): Observable<void> {
    this.loading$$.next(true);
    return this.authorsService.deleteAuthor(id).pipe(
      finalize(() => this.loading$$.next(false)),
      tap(() => this.authors$$.next(this.authors$$.value.filter(a => a.id !== id)))
    );
  }

  addAuthor(name: string): Observable<IAuthor> {
    this.loading$$.next(true);
    return this.authorsService.addAuthor(name).pipe(
      finalize(() => this.loading$$.next(false)),
      tap((author) => this.authors$$.next(this.authors$$.value.concat(author)))
    );
  }
}
